---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Generalized mutual information metrics for affine brain image registration: proposal and evaluation"
authors: ["Vinicius Pavanelli Vianna"]
date: 2019-09-24T19:15:20-03:00
doi: "10.11606/T.59.2021.tde-17122021-175100"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-10-20T19:15:20-03:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["7"]

# Publication name and optional abbreviated publication name.
publication: "Generalized mutual information metrics for affine brain image registration: proposal and evaluation"
publication_short: ""

abstract: "Background and Objective: Image registration is a key operation in medical image processing, allowing a plethora of applications. Mutual information (MI) is consolidated as a robust similarity metric often used for medical image registration. Although MI provides a robust registration, it usually fails when the transform needed to register an image is too big due to MI local maxima traps. Methods: In this paper, we propose and evaluate the Generalized MI (GMI), using Tsallis entropy as an affine registration metric with additive and non-additive equations. We assessed the metrics for separable affine transforms and exhaustively evaluated the GMI output signal seeking the maximum registration range through simulated gradient descent and a Monte Carlo simulation of real registrations with translated images. Results: GMI metrics showed smoother isosurfaces that better lead the algorithm to the solution. Results show significantly prolonged registration ranges, without local maxima in the metric space, evaluated through a range of 150 mm for translations, 360° for rotations, [0.5, 2] for scaling, and [-1, 1] for skewness, with a success rate of 99.99%, 88.20%, 99.99%, and 99.99%, respectively, in simulated gradient descent. We also obtained 99.75% success in the Monte Carlo simulation of 2,000 translation registrations with 1,113 double randomized subjects, using T1 and T2 brain MRI. Conclusions: The findings argue toward the reliability of GMI for long-range registrations. MI registration with Tsallis entropy requires specific entropic indexes (q) per transformation parameter and also a proper selection of additive and non-additive equation depending on the transformation type. Also, with parallel computing and more computational power, a better analysis of the signal present on images, without simplifications like voxel sampling or histogram binning, provides a more efficient and robust MI registration, as the Monte Carlo simulation shows."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
