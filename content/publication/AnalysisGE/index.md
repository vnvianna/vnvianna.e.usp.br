---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Long-range medical image registration through generalized mutual information (GMI): towards a fully automatic volumetric alignment"
authors: ["Vinicius Pavanelli Vianna", "Luiz Otavio Murta Junior"]
date: 2019-09-24T19:15:20-03:00
doi: "10.1088/1361-6560/ac5298"

# Schedule page publish date (NOT publication's date).
publishDate: 2019-10-20T19:15:20-03:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Long-range medical image registration through generalized mutual information (GMI): towards a fully automatic volumetric alignment"
publication_short: ""

abstract: "Objective. Mutual information (MI) is consolidated as a robust similarity metric often used for medical image registration. Although MI provides a robust registration, it usually fails when the transform needed to register an image is too large due to MI local minima traps. This paper proposes and evaluates Generalized MI (GMI), using Tsallis entropy, to improve affine registration. Approach. We assessed the GMI metric output space using separable affine transforms to seek a better gradient space. The range used was 150 mm for translations, 360° for rotations, [0.5, 2] for scaling, and [−1, 1] for skewness. The data were evaluated using 3D visualization of gradient and contour curves. A simulated gradient descent algorithm was also used to calculate the registration capability. The improvements detected were then tested through Monte Carlo simulation of actual registrations with brain T1 and T2 MRI from the HCP dataset. Main results. Results show significantly prolonged registration ranges, without local minima in the metric space, with a registration capability of 100% for translations, 88.2% for rotations, 100% for scaling and 100% for skewness. Tsallis entropy obtained 99.75% success in the Monte Carlo simulation of 2000 translation registrations with 1113 double randomized subjects T1 and T2 brain MRI against 56.5% success for the Shannon entropy. Significance. Tsallis entropy can improve brain MRI MI affine registration with long-range translation registration, lower-cost interpolation, and faster registrations through a better gradient space."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
