---
# Display name
name: Vinicius Pavanelli Vianna

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: PostDoc at DCM-FFCLRP-USP

# Organizations/Affiliations
organizations:
- name: FAMB - USP
  url: "https://sites.usp.br/famb/"

# Short bio (displayed in user profile at end of posts)
bio: Research in medical brain images registration using generalized entropies

interests:
- Mutual information registration
- Generalized entropies (Tsallis and Rényi)
- Artificial Inteligence
- Scientific computation (CUDA)

education:
  courses:
  - course: PhD in Physics applied to Medicine and Biology
    institution: FAMB/FFCLRP at University of São Paulo
    year: 2021
  - course: BSc in Telecomunications Engineering
    institution: Polytechnic School of the University of São Paulo
    year: 2011

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/vnpavanelli
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "vnpavanelli@wexperts.com.br"
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

Vinicius Pavanelli Vianna is a PostDoc PhD researcher at the CSIM lab (DCM-FFCLRP-USP). 
His research interests include medical image registration using mutual information, neural networks, machine learning algorithms and programming on CUDA and C++.
